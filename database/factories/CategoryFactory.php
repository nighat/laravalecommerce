<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'title'       => $faker->title,
        'created_by'       => $faker->word,
        'updated_by'       => $faker->word,
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];
});
