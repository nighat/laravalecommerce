<?php

use Faker\Generator as Faker;

use Carbon\Carbon;
$factory->define(App\Review::class, function (Faker $faker) {
    return [
        'detail'       => $faker->word,
        'created_by'       => $faker->word,
        'updated_by'       => $faker->word,
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];
});
