<?php

use Illuminate\Database\Seeder;

class StaticblocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Staticblock::class, 5)->create();
    }
}
