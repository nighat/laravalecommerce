@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')
                <div class="tt-page__name"><h1>Blog</h1></div>
                <div class="tt-post-grid__wrap tt-masonry row">
                    <div class="col-xl-4 col-lg-6">
                        <div class="tt-post-grid">
                            <a href="#" class="tt-post-grid__image ttg-image-translate--left">
                                <img src="images/blog/masonry/blog-masonry-01.jpg" alt="Image name">
                            </a>
                            <div class="tt-post-grid__content">
                                <div class="tt-post-grid__category">
                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                </div>
                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis iste natus sit
                                    voluptatem.</a>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
                                <div class="tt-post-grid__footer">
                                    <div class="tt-post-grid__info">
                                        <span>Robert</span> on December 28, 2017
                                        <div class="tt-post-grid__tags">
                                            <i class="icon-tag-1"></i>
                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                        </div>
                                    </div>
                                    <a href="#" class="tt-post-grid__comments">
                                        <i class="icon-comment-empty"></i><span>7</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6">
                        <div class="tt-post-grid">
                            <a href="#" class="tt-post-grid__image ttg-image-scale">
                                <img src="images/blog/masonry/blog-masonry-02.jpg" alt="Image name">
                            </a>
                            <div class="tt-post-grid__content">
                                <div class="tt-post-grid__category">
                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                </div>
                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis iste natus sit
                                    voluptatem.</a>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
                                <div class="tt-post-grid__footer">
                                    <div class="tt-post-grid__info">
                                        <span>Robert</span> on December 28, 2017
                                        <div class="tt-post-grid__tags">
                                            <i class="icon-tag-1"></i>
                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                        </div>
                                    </div>
                                    <a href="#" class="tt-post-grid__comments">
                                        <i class="icon-comment-empty"></i><span>7</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6">
                        <div class="tt-post-grid">
                            <a href="#" class="tt-post-grid__image ttg-image-translate--top">
                                <img src="images/blog/masonry/blog-masonry-03.jpg" alt="Image name">
                            </a>
                            <div class="tt-post-grid__content">
                                <div class="tt-post-grid__category">
                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                </div>
                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis iste natus sit
                                    voluptatem.</a>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
                                <div class="tt-post-grid__footer">
                                    <div class="tt-post-grid__info">
                                        <span>Robert</span> on December 28, 2017
                                        <div class="tt-post-grid__tags">
                                            <i class="icon-tag-1"></i>
                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                        </div>
                                    </div>
                                    <a href="#" class="tt-post-grid__comments">
                                        <i class="icon-comment-empty"></i><span>7</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6">
                        <div class="tt-post-grid">
                            <a href="#" class="tt-post-grid__image ttg-image-translate--right">
                                <img src="images/blog/masonry/blog-masonry-04.jpg" alt="Image name">
                            </a>
                            <div class="tt-post-grid__content">
                                <div class="tt-post-grid__category">
                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                </div>
                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis iste natus sit
                                    voluptatem.</a>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
                                <div class="tt-post-grid__footer">
                                    <div class="tt-post-grid__info">
                                        <span>Robert</span> on December 28, 2017
                                        <div class="tt-post-grid__tags">
                                            <i class="icon-tag-1"></i>
                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                        </div>
                                    </div>
                                    <a href="#" class="tt-post-grid__comments">
                                        <i class="icon-comment-empty"></i><span>7</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6">
                        <div class="tt-post-grid tt-post-grid__bg-theme">
                            <div class="tt-post-grid__content">
                                <div class="tt-post-grid__category">
                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                </div>
                                <div class="tt-post-grid__quote">
                                    <div><i class="icon-quote-1"></i></div>
                                    <div>
                                        <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis iste
                                            natus sit voluptatem.</a>
                                        <span class="tt-post-grid__signature">— Robert Trump</span>
                                    </div>
                                </div>
                                <div class="tt-post-grid__footer">
                                    <div class="tt-post-grid__info">
                                        <span>Robert</span> on December 28, 2017
                                        <div class="tt-post-grid__tags">
                                            <i class="icon-tag-1"></i>
                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                        </div>
                                    </div>
                                    <a href="#" class="tt-post-grid__comments">
                                        <i class="icon-comment-empty"></i><span>7</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6">
                        <div class="tt-post-grid">
                            <div class="tt-post-grid__content">
                                <div class="tt-post-grid__category">
                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                </div>
                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis iste natus sit
                                    voluptatem.</a>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
                                <div class="tt-post-grid__footer">
                                    <div class="tt-post-grid__info">
                                        <span>Robert</span> on December 28, 2017
                                        <div class="tt-post-grid__tags">
                                            <i class="icon-tag-1"></i>
                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                        </div>
                                    </div>
                                    <a href="#" class="tt-post-grid__comments">
                                        <i class="icon-comment-empty"></i><span>7</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6">
                        <div class="tt-post-grid">
                            <div class="tt-post-grid__image">
                                <div class="tt-post-grid__slider">
                                    <img src="images/blog/masonry/blog-masonry-05.jpg" alt="Image name">
                                    <img src="images/blog/masonry/blog-masonry-01.jpg" alt="Image name">
                                    <img src="images/blog/masonry/blog-masonry-04.jpg" alt="Image name">
                                </div>
                                <div class="tt-post-grid__slider-nav"></div>
                            </div>
                            <div class="tt-post-grid__content">
                                <div class="tt-post-grid__category">
                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                </div>
                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis iste natus sit
                                    voluptatem.</a>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
                                <div class="tt-post-grid__footer">
                                    <div class="tt-post-grid__info">
                                        <span>Robert</span> on December 28, 2017
                                        <div class="tt-post-grid__tags">
                                            <i class="icon-tag-1"></i>
                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                        </div>
                                    </div>
                                    <a href="#" class="tt-post-grid__comments">
                                        <i class="icon-comment-empty"></i><span>7</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6">
                        <div class="tt-post-grid">
                            <a href="#" class="tt-post-grid__image ttg-image-translate--bottom">
                                <img src="images/blog/grid/blog-grid-06.jpg" alt="Image name">
                            </a>
                            <div class="tt-post-grid__content">
                                <div class="tt-post-grid__category">
                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                </div>
                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis iste natus sit
                                    voluptatem.</a>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
                                <div class="tt-post-grid__footer">
                                    <div class="tt-post-grid__info">
                                        <span>Robert</span> on December 28, 2017
                                        <div class="tt-post-grid__tags">
                                            <i class="icon-tag-1"></i>
                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                        </div>
                                    </div>
                                    <a href="#" class="tt-post-grid__comments">
                                        <i class="icon-comment-empty"></i><span>7</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6">
                        <div class="tt-post-grid tt-post-grid__bg-dark">
                            <div class="tt-post-grid__content">
                                <div class="tt-post-grid__category">
                                    <a href="listing-with-custom-html-block.html">Headphones</a>
                                </div>
                                <a href="#" class="tt-post-grid__title">Sed ut perspiciatis unde omnis iste natus sit
                                    voluptatem.</a>
                                <div class="tt-post-grid__footer">
                                    <div class="tt-post-grid__info">
                                        <span>Robert</span> on December 28, 2017
                                        <div class="tt-post-grid__tags">
                                            <i class="icon-tag-1"></i>
                                            <a href="listing-with-custom-html-block.html">Audio,</a>
                                            <a href="listing-with-custom-html-block.html">Headphones</a>
                                        </div>
                                    </div>
                                    <a href="#" class="tt-post-grid__comments">
                                        <i class="icon-comment-empty"></i><span>7</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tt-page__pagination">
                    <div class="tt-pagination">
                        <a href="#" class="btn tt-pagination__prev ttg-hidden">Prev</a>
                        <div class="tt-pagination__numbs">
                            <span>1</span>
                            <span><a href="#">2</a></span>
                            <span><a href="#">3</a></span>
                        </div>
                        <a href="#" class="btn tt-pagination__next">Next</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
@endsection