@extends('layouts/mogo')

@section('main_content')
    <div class="panel-heading">
        <h5 style="text-align: center">
            Category Lists
            <a href="{{ route('category.create') }}" class=" btn btn-danger pull-right"> Add New Category</a>

        </h5></div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Title</th>
            <th>Created_by</th>
            <th>Updated_by</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>

        @foreach( $categories as $category )


            <tr>
                <td>{{ $loop->index + 1}}</td>
                <td>{{ $category-> title }}

                </td>
                <td>{{ $category-> created_by }}</td>
                <td>  {{ $category-> updated_by }}</td>
                <td> <a href="{{ route('category.show',$category->id) }}" class=" btn btn-info"><span class=" glyphicon glyphicon-eye-open"></span></a>

                    <a href="{{ route('category.edit',$category->id) }}" class=" btn btn-info"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $category->id }}" method="POST" action="{{ route('category.destroy',$category->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $category->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-info"><span class="glyphicon glyphicon-trash"></span></a>

                </td>
            </tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th>S.No</th>
            <th>Title</th>
            <th>Created_by</th>
            <th>Updated_by</th>
            <th>Action</th>




        </tr>
        </tfoot>
    </table>


@endsection