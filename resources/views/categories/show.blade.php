

@extends('layouts/mogo')

@section('main_content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">


                    <div class="panel-heading ">
                        <h5 style="text-align: center">Category Details
                            <a href="{{ route('category.create') }}" class=" btn btn-danger pull-right">Create</a>
                            <a href="{{ route('category.index') }}" class=" btn btn-danger pull-right">Category</a>
                        </h5>

                    </div>

                    <div>
                        <b>Title:-</b>  {{ $categories->title}}
                    </div>
                    <div>
                        <b>Created By:-</b>    {{ $categories->created_by}}
                    </div>
                    <div>
                        <b>Updated By:-</b>    {{ $categories->updated_by}}
                    </div>
                </div>

        </div>
    </div>
@endsection