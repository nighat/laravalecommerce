@extends('layouts/mogo')

@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                    <div class="panel-heading">
                        <h3>Product Create Form</h3>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('product.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="image" class="col-md-4 control-label">Image</label>

                                <div class="col-md-6">
                                    <input id="image" type="file" class="form-control" name="image" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="short_description" class="col-md-4 control-label">Short Description</label>

                                <div class="col-md-6">
                                    <input id="short_description" type="text" class="form-control" name="short_description" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-md-4 control-label">
                                    Description</label>

                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="additional_information" class="col-md-4 control-label">
                                   Additional Information</label>

                                <div class="col-md-6">
                                    <input id="additional_information" type="text" class="form-control" name="additional_information" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="price" class="col-md-4 control-label">
                                    Price</label>

                                <div class="col-md-6">
                                    <input id="price" type="number" class="form-control" name="price" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="special_price" class="col-md-4 control-label">
                                    Special Price</label>

                                <div class="col-md-6">
                                    <input id="special_price" type="number" class="form-control" name="special_price" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="qty" class="col-md-4 control-label">
                                    Qty</label>

                                <div class="col-md-6">
                                    <input id="qty" type="number" class="form-control" name="qty" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="start_date" class="col-md-4 control-label">
                                    Start Date</label>

                                <div class="col-md-6">
                                    <input id="start_date" type="date" class="form-control" name="start_date" >
                                </div>
                            </div>

                           <div class="form-group">
                                <label for="end_date" class="col-md-4 control-label">
                                    End Date</label>

                                <div class="col-md-6">
                                    <input id="end_date" type="date" class="form-control" name="end_date" >
                                </div>
                            </div>

                           <div class="form-group">
                                <label for="sku" class="col-md-4 control-label">
                                    SKU</label>

                                <div class="col-md-6">
                                    <input id="sku" type="text" class="form-control" name="sku" >
                                </div>
                            </div>

                           <div class="form-group">
                                <label for="meta_keyword" class="col-md-4 control-label">
                                    Meta Keyword</label>

                                <div class="col-md-6">
                                    <input id="meta_keyword" type="text" class="form-control" name="meta_keyword" >
                                </div>
                            </div>

                           <div class="form-group">
                                <label for="meta_description" class="col-md-4 control-label">
                                    Meta Description</label>

                                <div class="col-md-6">
                                    <input id="meta_description" type="text" class="form-control" name="meta_description" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="product_url" class="col-md-4 control-label">
                                    Product URL</label>

                                <div class="col-md-6">
                                    <input id="product_url" type="text" class="form-control" name="product_url" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="created_by" class="col-md-4 control-label">Created By</label>

                                <div class="col-md-6">
                                    <input id="created_by" type="text" class="form-control" name="created_by" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="updated_by" class="col-md-4 control-label">Updated By</label>

                                <div class="col-md-6">
                                    <input id="updated_by" type="text" class="form-control" name="updated_by" >
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">

                                    <button type="submit" class="btn btn-primary">Submit  </button>
                                    <a href="{{ route('product.index') }}" class=" btn btn-danger">Back</a>

                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>
@endsection