@extends('layouts/mogo')

@section('main_content')
    <div class="panel-heading">
        <h5 style="text-align: center">Slider Lists <a href="{{ route('sliders.create') }}" class=" btn btn-danger pull-right">Add New Slider</a></h5>
        <h5 >{{ session('message') }}</h5>


    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Image</th>
            <th>content</th>
            <th>Link</th>
            <th>HTML_Blog</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>

        @foreach($sliders as $slider)

            <tr>
                <td>{{ $loop->index + 1}}</td>

                <td><a href=" {{ $slider->link }}"><img style="height: 200px; width: 200px" src="{{asset('storage/'.$slider->image)}}"></a></td>
                <td>{{ $slider->contant }}</td>
                <td>{{ $slider->link }}</td>
                <td>{{ $slider->html_blog}}</td>
                <td> <a href="{{ route('sliders.show',$slider->id) }}" class=" btn btn-danger"><span class=" glyphicon glyphicon-eye-open"></span></a>
                    <a href="{{ route('sliders.edit',$slider->id) }}" class=" btn btn-info"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $slider->id }}" method="POST" action="{{ route('sliders.destroy',$slider->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $slider->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-info"><span class="glyphicon glyphicon-trash"></span></a>

                </td>


            </tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th>S.No</th>
            <th>Picture</th>
            <th>Content</th>
            <th>Link</th>
            <th>HTML_Blog</th>
            <th>Action</th>



        </tr>
        </tfoot>
    </table>



@endsection