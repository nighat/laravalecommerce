@extends('layouts/mogo')

@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">

                    <div class="panel-heading"><h3 style="text-align: center">Staticblock Create Form</h3>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('staticblock.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="contant" class="col-md-4 control-label">Contant</label>

                                <div class="col-md-6">
                                    <input id="contant" type="text" class="form-control" name="contant" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="created_by" class="col-md-4 control-label">Created By</label>

                                <div class="col-md-6">
                                    <input id="created_by" type="text" class="form-control" name="created_by" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="updated_by" class="col-md-4 control-label">Updated By</label>

                                <div class="col-md-6">
                                    <input id="updated_by" type="text" class="form-control" name="updated_by" >
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('staticblock.index') }}" class=" btn btn-danger">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>

            </div>
        </div>
    </div>
@endsection