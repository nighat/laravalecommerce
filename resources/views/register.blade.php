@extends('layouts/mogo')


@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <a href="index.html?page=login.html">Login</a>
                    <span>/</span>
                    <h1>Register</h1>
                </div>

                <div class="tt-login">
                    <div class="tt-login__title">
                        <p>Personal Information</p>
                    </div>
                    <form action="#">
                        <div class="tt-form">
                            <div class="tt-form__form">
                                <label>
                                    <div class="row">
                                        <div class="col-md-3"><span class="ttg__required">First Name:</span></div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control"
                                                   placeholder="Enter please your first name">
                                        </div>
                                    </div>
                                </label>
                                <label>
                                    <div class="row">
                                        <div class="col-md-3"><span class="ttg__required">Last Name:</span></div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control"
                                                   placeholder="Enter please your last name">
                                        </div>
                                    </div>
                                </label>
                                <label>
                                    <div class="row">
                                        <div class="col-md-3"><span class="ttg__required">Email:</span></div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control"
                                                   placeholder="Enter please your name">
                                        </div>
                                    </div>
                                </label>
                                <label>
                                    <div class="row">
                                        <div class="col-md-3"><span class="ttg__required">Password:</span></div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control"
                                                   placeholder="Enter please your password">
                                        </div>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9 offset-md-3 ttg-mt--40">
                                <button type="submit" class="btn">Create</button>
                                <div class="tt-login__tostore">
                                    <span>or</span>
                                    <a href="#">Return to Store</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
@endsection