@extends ('layouts/mogo')
<!-- MAIN -->

@section('main_content')
<main>
    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                <div class="tt-sr" data-layout="fullscreen">
                    <div class="tt-sr__content" data-version="5.3.0.2">
                        <ul>
                            <li data-index="rs-3045"
                                data-transition="parallaxvertical"
                                data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
                                data-masterspeed="1500">
                                <img src="images/slider/01/slide-01.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                                rs-parallaxlevel-3
                                tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[-100%];opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Speaker Bottle</div>
                                    <span>$121</span>
                                    <p>Portable Bluetooth stereo speakers</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3046"
                                data-transition="fade"
                                data-masterspeed="1500">
                                <img src="images/slider/01/slide-02.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-kenburns="on"
                                     data-duration="10000"
                                     data-ease="Linear.easeNone"
                                     data-scalestart="120"
                                     data-scaleend="100"
                                     data-offsetstart="0 0"
                                     data-offsetend="0 0"
                                     data-rotatestart="0"
                                     data-rotateend="0"
                                     data-bgparallax="8">
                                <div class="tp-caption
                            rs-parallaxlevel-3
                            tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"1999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Smart Watches</div>
                                    <span>$349</span>
                                    <p>Just what you need. Right when you need it.</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3047"
                                data-transition="parallaxhorizontal"
                                data-masterspeed="1500">
                                <img src="images/slider/01/slide-03.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                            rs-parallaxlevel-3
                            tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Power Bank</div>
                                    <span>$49</span>
                                    <p>Elegant and fresh. A most attractive mobile power
                                        supply.</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3048"
                                data-transition="zoomout"
                                data-masterspeed="1500">
                                <img src="images/slider/01/slide-04.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                            rs-parallaxlevel-3
                            tt-sr__text
                            tt-sr__design-white"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"1999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Tracker</div>
                                    <span>$65</span>
                                    <p>Cheap Fitness Tracker</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3049"
                                data-transition="parallaxvertical"
                                data-masterspeed="1500">
                                <img src="images/slider/01/slide-05.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                            rs-parallaxlevel-3
                            tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Wireless Speaker</div>
                                    <span>$78</span>
                                    <p>Portable Bluetooth stereo speakers</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3050"
                                data-transition="fade"
                                data-masterspeed="1500">
                                <img src="images/slider/01/slide-06.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-kenburns="on"
                                     data-duration="6000"
                                     data-ease="Linear.easeNone"
                                     data-scalestart="120"
                                     data-scaleend="100"
                                     data-offsetstart="0 0"
                                     data-offsetend="0 0"
                                     data-rotatestart="0"
                                     data-rotateend="0"
                                     data-bgparallax="8">
                                <div class="tp-caption
                            rs-parallaxlevel-3
                            tt-sr__text
                            tt-sr__design-white"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Headphone</div>
                                    <span>$127</span>
                                    <p>Superior sound quality</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3051"
                                data-transition="fade"
                                data-masterspeed="1500">
                                <div class="tp-caption tp-resizeme tp-videolayer"
                                     data-frames='[{"delay": 500, "speed": 300, "from": "opacity: 0", "to": "opacity: 1"}, {"delay": "wait", "speed": 300, "to": "opacity: 0"}]'
                                     data-type="video"
                                     data-videomp4="images/slider/video/mp4/video.mp4"
                                     data-videowidth="100%"
                                     data-videoheight="auto"
                                     data-autoplay="on"
                                     data-videocontrols="none"
                                     data-nextslideatend="true"
                                     data-forcerewind="on"
                                     data-videoloop="loopandnoslidestop"
                                     data-allowfullscreenvideo="true"
                                     data-videopreload="auto"
                                     data-x="center"
                                     data-y="center"
                                     data-hoffset="0"
                                     data-voffset="0"
                                     data-basealign="slide">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="tt-home__promobox-01">
                    <div class="container-fluid ttg-cont-padding--none">
                        <div class="row ttg-grid-padding--none">
                            <div class="col-sm-8">
                                <div class="row ttg-grid-padding--none">
                                    <div class="col-sm-6">
                                        <a href="#" class="tt-promobox
                                                           ttg-text-animation-parent
                                                           ttg-image-translate--right
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md"
                                        >
                                            <div class="tt-promobox__content">
                                                <img src="#" data-srcset="images/promoboxes/promobox-01.jpg"
                                                     alt="Image name">
                                                <div class="tt-promobox__text"
                                                     data-resp-md="md"
                                                     data-resp-sm="md"
                                                     data-resp-xs="sm">
                                                    <div>Watches</div>
                                                </div>
                                                <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                    <div class="tt-promobox__hover-bg"></div>
                                                    <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                        <div class="ttg-text-animation--emersion">
                                                            <span>Watches</span>
                                                        </div>
                                                        <p class="ttg-text-animation--emersion">
                                                            <span><span>28</span> products</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="#" class="tt-promobox
                                                           ttg-text-animation-parent
                                                           ttg-image-translate--top
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md">
                                            <div class="tt-promobox__content">
                                                <img src="#" data-srcset="images/promoboxes/promobox-02.jpg"
                                                     alt="Image name">
                                                <div class="tt-promobox__text"
                                                     data-resp-md="md"
                                                     data-resp-sm="md"
                                                     data-resp-xs="sm">
                                                    <div class="colorize-theme2-c">Trackers</div>
                                                </div>
                                                <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                    <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                                    <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                        <div class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c">Trackers</span>
                                                        </div>
                                                        <p class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c"><span>46</span> products</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-12">
                                        <a href="#" class="tt-promobox
                                                           tt-promobox__size-wide
                                                           ttg-text-animation-parent
                                                           ttg-image-scale
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md">
                                            <div class="tt-promobox__content">
                                                <img src="#" data-srcset="images/promoboxes/promobox-03.jpg"
                                                     alt="Image name">
                                                <div class="tt-promobox__text"
                                                     data-resp-md="md"
                                                     data-resp-sm="md"
                                                     data-resp-xs="sm">
                                                    <div class="colorize-theme2-c">Headphones</div>
                                                </div>
                                                <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                    <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                                    <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                        <div class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c">Headphones</span>
                                                        </div>
                                                        <p class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c">32 products</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" class="tt-promobox
                                                   tt-promobox__size-high
                                                   ttg-text-animation-parent
                                                   ttg-image-translate--left
                                                   ttg-animation-disable--md
                                                   tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img src="#" data-srcset="images/promoboxes/promobox-04.jpg" alt="Image name">
                                        <div class="tt-promobox__text"
                                             data-resp-md="md"
                                             data-resp-sm="md"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Earphones</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Earphones</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">24 products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid ttg-cont-padding--none">
                    <div class="row tt-product-view ttg-grid-padding--none">
                        <div class="col-sm-6 col-xl-3">
                            <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                <div class="tt-product__image">
                                    <a href="product-simple-variant-1">
                                        <img src="images/loader.svg" data-srcset="images/products/product-01.jpg"
                                             data-retina="images/products/product-01.jpg"
                                             alt="Elegant and fresh. A most attractive mobile power supply.">
                                    </a>
                                    <div class="tt-product__labels">
                                        <span class="tt-label__new">New</span>
                                        <span class="tt-label__hot">Hot</span>

                                        <span class="tt-label__sale">Sale</span>
                                        <span class="tt-label__discount">$22</span>

                                        <div><span class="tt-label__out-stock">Out Stock</span></div>
                                        <div><span class="tt-label__in-stock">In Stock</span></div>
                                    </div>
                                </div>
                                <div class="tt-product__hover tt-product__clr-clk-transp">
                                    <div class="tt-product__content">
                                        <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block">Headphones</a>
                                            </span>
                                        </h3>
                                        <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                        </p>
                                        <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                            eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                            aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                        <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$22</span>
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                        </div>

                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__stars tt-stars">
                                                <span class="ttg-icon"></span>
                                                <span class="ttg-icon" style="width:86%;"></span>
                                            </div>
                                        </div>
                                        <div class="tt-product__option">
                                            <div class="prdbut__options prdbut__options--list">
                                                <div class="ttg-text-animation--emersion">
                                                    <div class="prdbut__option prdbut__option--color prdbut__option--design-color">
                                                        <span class="prdbut__val prdbut__val--white active default">
                                                            <span>White</span>
                                                        </span>
                                                        <span class="prdbut__val prdbut__val--black">
                                                            <span>Black</span>
                                                        </span>
                                                        <span class="prdbut__val prdbut__val--red">
                                                            <span>Red</span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="ttg-text-animation--emersion">
                                                    <div class="prdbut__option prdbut__option--size prdbut__option--design-bg">
                                                        <span class="prdbut__val prdbut__val--m active default">
                                                            <span>M</span>
                                                        </span>
                                                        <span class="prdbut__val prdbut__val--l">
                                                            <span>L</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__buttons">
                                                <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart tt-btn__state--active">
                                                    <i class="icon-shop24"></i>
                                                    <span>Add to Cart</span>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like tt-btn__state--active">
                                                    <i class="icon-heart-empty-2"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare tt-btn__state--active">
                                                    <i class="icon-untitled-1"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                    <i class="icon-eye"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__countdown" data-date="2018-10-01 20:00:00" data-zone="Europe/Madrid"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                <div class="tt-product__image">
                                    <a href="product-simple-variant-1">
                                        <img src="images/loader.svg" data-srcset="images/products/product-02.jpg"
                                             data-retina="images/products/product-02.jpg"
                                             alt="Elegant and fresh. A most attractive mobile power supply.">
                                    </a>
                                </div>
                                <div class="tt-product__hover tt-product__clr-clk-transp">
                                    <div class="tt-product__content">
                                        <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block">Phone</a>
                                            </span>
                                        </h3>
                                        <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                        </p>
                                        <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                            eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                            aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                        <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$46</span>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__stars tt-stars">
                                                <span class="ttg-icon"></span>
                                                <span class="ttg-icon" style="width:35%;"></span>
                                            </div>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__buttons">
                                                <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                    <i class="icon-shop24"></i>
                                                    <span>Add to Cart</span>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                    <i class="icon-heart-empty-2"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                    <i class="icon-untitled-1"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                    <i class="icon-eye"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                <div class="tt-product__image">
                                    <a href="product-simple-variant-1">
                                        <img src="images/loader.svg" data-srcset="images/products/product-03.jpg"
                                             data-retina="images/products/product-03.jpg"
                                             alt="Elegant and fresh. A most attractive mobile power supply.">
                                    </a>
                                    <div class="tt-product__labels">
                                        <span class="tt-label__sale">Sale</span>
                                        <span class="tt-label__discount">$32</span>
                                    </div>
                                </div>
                                <div class="tt-product__hover tt-product__clr-clk-transp">
                                    <div class="tt-product__content">
                                        <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block">USB</a>
                                            </span>
                                        </h3>
                                        <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1>Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                        </p>
                                        <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                            eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                            aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                        <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price tt-price--sale">
                                                    <span>$32</span>
                                                    <span>$38</span>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__buttons">
                                                <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                    <i class="icon-shop24"></i>
                                                    <span>Add to Cart</span>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                    <i class="icon-heart-empty-2"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                    <i class="icon-untitled-1"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                    <i class="icon-eye"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__countdown" data-date="2018-06-01"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                <div class="tt-product__image">
                                    <a href="product-simple-variant-1">
                                        <img src="images/loader.svg" data-srcset="images/products/product-04.jpg"
                                             data-retina="images/products/product-04.jpg"
                                             alt="Elegant and fresh. A most attractive mobile power supply.">
                                    </a>
                                </div>
                                <div class="tt-product__hover tt-product__clr-clk-transp">
                                    <div class="tt-product__content">
                                        <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block">Category</a>
                                            </span>
                                        </h3>
                                        <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                        </p>
                                        <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                            eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                            aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                        <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$14</span>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__buttons">
                                                <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                    <i class="icon-shop24"></i>
                                                    <span>Add to Cart</span>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                    <i class="icon-heart-empty-2"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                    <i class="icon-untitled-1"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                    <i class="icon-eye"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                <div class="tt-product__image">
                                    <a href="product-simple-variant-1">
                                        <img src="images/loader.svg" data-srcset="images/products/product-05.jpg"
                                             data-retina="images/products/product-05.jpg"
                                             alt="Elegant and fresh. A most attractive mobile power supply.">
                                    </a>
                                </div>
                                <div class="tt-product__hover tt-product__clr-clk-transp">
                                    <div class="tt-product__content">
                                        <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block">Category</a>
                                            </span>
                                        </h3>
                                        <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                        </p>
                                        <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                            eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                            aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                        <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$24</span>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__buttons">
                                                <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                    <i class="icon-shop24"></i>
                                                    <span>Add to Cart</span>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                    <i class="icon-heart-empty-2"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                    <i class="icon-untitled-1"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                    <i class="icon-eye"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                <div class="tt-product__image">
                                    <a href="product-simple-variant-1">
                                        <img src="images/loader.svg" data-srcset="images/products/product-06.jpg"
                                             data-retina="images/products/product-06.jpg"
                                             alt="Elegant and fresh. A most attractive mobile power supply.">
                                    </a>
                                </div>
                                <div class="tt-product__hover tt-product__clr-clk-transp">
                                    <div class="tt-product__content">
                                        <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block">Category</a>
                                            </span>
                                        </h3>
                                        <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                        </p>
                                        <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                            eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                            aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                        <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$28</span>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__buttons">
                                                <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                    <i class="icon-shop24"></i>
                                                    <span>Add to Cart</span>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                    <i class="icon-heart-empty-2"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                    <i class="icon-untitled-1"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                    <i class="icon-eye"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                <div class="tt-product__image">
                                    <a href="product-simple-variant-1">
                                        <img src="images/loader.svg" data-srcset="images/products/product-07.jpg"
                                             data-retina="images/products/product-07.jpg"
                                             alt="Elegant and fresh. A most attractive mobile power supply.">
                                    </a>
                                </div>
                                <div class="tt-product__hover tt-product__clr-clk-transp">
                                    <div class="tt-product__content">
                                        <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block">Category</a>
                                            </span>
                                        </h3>
                                        <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                        </p>
                                        <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                            eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                            aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                        <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$58</span>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__buttons">
                                                <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                    <i class="icon-shop24"></i>
                                                    <span>Add to Cart</span>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                    <i class="icon-heart-empty-2"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                    <i class="icon-untitled-1"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                    <i class="icon-eye"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="tt-product tt-product__view-overlay ttg-text-animation-parent">
                                <div class="tt-product__image">
                                    <a href="product-simple-variant-1">
                                        <img src="images/loader.svg" data-srcset="images/products/product-08.jpg"
                                             data-retina="images/products/product-08.jpg"
                                             alt="Elegant and fresh. A most attractive mobile power supply.">
                                    </a>
                                </div>
                                <div class="tt-product__hover tt-product__clr-clk-transp">
                                    <div class="tt-product__content">
                                        <h3>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="listing-with-custom-html-block">Category</a>
                                            </span>
                                        </h3>
                                        <p>
                                            <span class="ttg-text-animation--emersion">
                                                <a href="product-simple-variant-1">Elegant and fresh. A most attractive mobile power supply.</a>
                                            </span>
                                        </p>
                                        <p class="tt-product__description">Sed ut perspiciatis unde omnis iste natus
                                            error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                            eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                                            aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>
                                        <div class="ttg-text-animation--emersion">
                                            <span class="tt-product__price">
                                                <span class="tt-price">
                                                    <span>$19</span>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="ttg-text-animation--emersion">
                                            <div class="tt-product__buttons">
                                                <a href="#" class="tt-btn colorize-btn5 tt-product__buttons_cart">
                                                    <i class="icon-shop24"></i>
                                                    <span>Add to Cart</span>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_like">
                                                    <i class="icon-heart-empty-2"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_compare">
                                                    <i class="icon-untitled-1"></i>
                                                </a>
                                                <a href="#" class="tt-btn colorize-btn4 tt-product__buttons_qv">
                                                    <i class="icon-eye"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tt-home__shipping-info-01">
                    <div class="tt-shp-info tt-shp-info__design-01">
                        <div class="row ttg-grid-padding--none ttg-grid-border">
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <i class="icon-phone"></i>
                                    <div class="tt-shp-info__strong">+(777) 2345 7885</div>
                                    <p>Toll-free hotline. 7 days a week from <strong><em>10.00 a.m. to 6.00
                                                p.m.</em></strong></p>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <i class="icon-box"></i>
                                    <div class="tt-shp-info__strong">Free Shipping</div>
                                    <p>Shipping prices for any form of delivery and order’s cost is constant - $49. A
                                        free shipping is available for orders <span>more than $99.</span></p>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <i class="icon-left"></i>
                                    <div class="tt-shp-info__strong">Returns and Exchanges</div>
                                    <p>Any goods, that was bought in our online store, can be returned during <span>30 days</span>
                                        since purchase date.</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
    @endsection

