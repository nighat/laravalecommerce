@extends ('layouts/mogo')


@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>Buttons</h1>
                </div>

                <div class="container">
                    <div class="tt-page__cont-small ttg-mt--60 ttg-mb--70 text-center">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#" class="btn btn--lg btn-type--icon colorize-btn6 ttg-mb--30">
                                    <i class="icon-shop24"></i>
                                    <span>Add to Cart</span>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="btn btn--lg colorize-btn6 ttg-mb--30">Add to Cart</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="btn btn--sm btn-type--icon colorize-btn6 ttg-mb--30">
                                    <i class="icon-shop24"></i>
                                    <span>Add to Cart</span>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="btn btn--sm colorize-btn6 ttg-mb--30">Add to Cart</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#" class="btn btn--lg btn-type--icon ttg-mb--30">
                                    <i class="icon-shop24"></i>
                                    <span>Add to Cart</span>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="btn btn--lg ttg-mb--30">Add to Cart</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="btn btn--sm btn-type--icon ttg-mb--30">
                                    <i class="icon-shop24"></i>
                                    <span>Add to Cart</span>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="btn btn--sm ttg-mb--30">Add to Cart</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tt-elements__img-bnts text-center">
                    <img src="images/elements/elements-buttons.jpg" alt="Image name">
                    <div>
                        <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                            <i class="icon-shop24"></i>
                        </a>
                        <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn8">
                            <i class="icon-shop24"></i>
                        </a>
                    </div>
                </div>
                <script>
                    require(['app'], function () {
                        require(['modules/listDropdown']);
                    });
                </script>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection