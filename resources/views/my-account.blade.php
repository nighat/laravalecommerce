@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>My Account</h1>
                </div>

                <div class="tt-my-account">
                    <div class="tt-my-account__tabs tt-tabs tt-layout__mobile-full" data-tt-type="vertical">
                        <div class="tt-tabs__head">
                            <div class="tt-tabs__slider">
                                <div class="tt-tabs__btn"><span>Dashboard</span></div>
                                <div class="tt-tabs__btn"><span>Orders</span></div>
                                <div class="tt-tabs__btn"><span>Downloads</span></div>
                                <div class="tt-tabs__btn"><span>Addresses</span></div>
                                <div class="tt-tabs__btn" data-active="true"><span>Account Details</span></div>
                                <div class="tt-tabs__btn"><span>Logout</span></div>
                            </div>
                            <div class="tt-tabs__btn-prev"></div>
                            <div class="tt-tabs__btn-next"></div>
                            <div class="tt-tabs__border"></div>
                        </div>
                        <div class="tt-tabs__body tt-tabs-my-account">
                            <div>
                                <span>Dashboard <i class="icon-down-open"></i></span>
                                <div class="tt-tabs__content">
                                    <p class="ttg-mb--20">Hello NAME (not name? <a href="#">Sign out</a>)</p>
                                    <p>From your account dashboard you can view your <a href="#">recent orders,</a>
                                        manage your <a href="#">shipping and billing addresses</a> and <a href="#">edit
                                            your password and account details.</a></p>
                                </div>
                            </div>
                            <div>
                                <span>Orders <i class="icon-down-open"></i></span>
                                <div class="tt-tabs__content">
                                    <h4>Orders</h4>
                                    <div class="tt-tabs-my-account__table ttg-mb--30">
                                        <table class="table">
                                            <tr>
                                                <th>Products</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Total</th>
                                            </tr>
                                            <tr>
                                                <td><a href="#">#001</a></td>
                                                <td>November 20. 2016</td>
                                                <td>Processing</td>
                                                <td>$40 fot 1 item</td>
                                                <td class="text-right"><a href="#">View</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">#002</a></td>
                                                <td>November 20. 2016</td>
                                                <td>Processing</td>
                                                <td>$40 fot 1 item</td>
                                                <td class="text-right"><a href="#">View</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">#003</a></td>
                                                <td>November 20. 2016</td>
                                                <td>Processing</td>
                                                <td>$40 fot 1 item</td>
                                                <td class="text-right"><a href="#">View</a></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <a href="#" class="btn">Previous</a>
                                    <a href="#" class="btn">Next</a>
                                    <p class="ttg-mt--30 ttg-mb--30">Order <a href="#">#391</a> was places on November
                                        20, 2016 and is currently Processing</p>
                                    <h4>Order Details</h4>
                                    <div class="tt-tabs-my-account__table ttg-mb--30">
                                        <table class="table tt-tabs-my-account--table-sm">
                                            <tr>
                                                <th>Product</th>
                                                <th>Total</th>
                                            </tr>
                                            <tr>
                                                <td><a href="#">Nike Zoom Trail</a> x 1</td>
                                                <td>$40</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Subtotal:</td>
                                                <td>$40</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Shipping:</td>
                                                <td>Free Shipping</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Payment Method:</td>
                                                <td>Cashon Delivery</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Total:</td>
                                                <td>$40</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <h4>Customer Details</h4>
                                    <div class="tt-tabs-my-account__table ttg-mb--30">
                                        <table class="table tt-tabs-my-account--table-sm">
                                            <tr>
                                                <td class="ttg-color--gray-dark">E-mail:</td>
                                                <td>Admin@localhost.dev</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Telephone:</td>
                                                <td>888-8888888</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <h4>Billing Address</h4>
                                    <div class="tt-tabs-my-account__table ttg-mb--30">
                                        <table class="table tt-tabs-my-account--table-sm">
                                            <tr>
                                                <td class="ttg-color--gray-dark">Country:</td>
                                                <td>Lorem ipsum dolor sit amet conse ctetur</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Address:</td>
                                                <td>Ut enim ad minim veniam, quis nostrud</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Town/City:</td>
                                                <td>Eexercitation ullamco laboris nisi ut aliquip ex ea</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">State/Country:</td>
                                                <td>Commodo consequat. Duis aute irure dol</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Postode/ZIP:</td>
                                                <td>88888</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <h4>Shipping Address</h4>
                                    <div class="tt-tabs-my-account__table ttg-mb--30">
                                        <table class="table tt-tabs-my-account--table-sm">
                                            <tr>
                                                <td class="ttg-color--gray-dark">Country:</td>
                                                <td>Lorem ipsum dolor sit amet conse ctetur</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Address:</td>
                                                <td>Ut enim ad minim veniam, quis nostrud</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Town/City:</td>
                                                <td>Eexercitation ullamco laboris nisi ut aliquip ex ea</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">State/Country:</td>
                                                <td>Commodo consequat. Duis aute irure dol</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Postode/ZIP:</td>
                                                <td>88888</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <span>Downloads <i class="icon-down-open"></i></span>
                                <div class="tt-tabs__content">
                                    <h4>Downloads</h4>
                                    <div class="tt-tabs-my-account__table ttg-mb--30">
                                        <table class="table ttg-mb--30">
                                            <tr>
                                                <th>File</th>
                                                <th>Remaining</th>
                                                <th>Expires</th>
                                            </tr>
                                            <tr>
                                                <td><a href="#">Downloadeble Product - File</a></td>
                                                <td>15</td>
                                                <td>Never</td>
                                                <td class="text-right"><a href="#">Download</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">Downloadeble Product - File</a></td>
                                                <td>15</td>
                                                <td>November 25, 2016</td>
                                                <td class="text-right"><a href="#">Download</a></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <span>Addresses <i class="icon-down-open"></i></span>
                                <div class="tt-tabs__content">
                                    <p class="ttg-mb--30">The following addresses will be used on the checkout page by
                                        default.</p>
                                    <div class="tt-tabs-my-account__head-edit">
                                        <h4>Billing Address</h4>
                                        <a href="#"><i class="icon-pencil-circled"></i>Edit</a>
                                    </div>
                                    <div class="tt-tabs-my-account__table ttg-mb--10">
                                        <table class="table tt-tabs-my-account--table-sm">
                                            <tr>
                                                <td class="ttg-color--gray-dark">Country:</td>
                                                <td>Lorem ipsum dolor sit amet conse ctetur</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Address:</td>
                                                <td>Ut enim ad minim veniam, quis nostrud</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Town/City:</td>
                                                <td>Eexercitation ullamco laboris nisi ut aliquip ex ea</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">State/Country:</td>
                                                <td>Commodo consequat. Duis aute irure dol</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Postode/ZIP:</td>
                                                <td>88888</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="tt-tabs-my-account__head-edit">
                                        <h4>Shipping Address</h4>
                                        <a href="#"><i class="icon-pencil-circled"></i>Edit</a>
                                    </div>
                                    <div class="tt-tabs-my-account__table ttg-mb--10">
                                        <table class="table tt-tabs-my-account--table-sm">
                                            <tr>
                                                <td class="ttg-color--gray-dark">Country:</td>
                                                <td>Lorem ipsum dolor sit amet conse ctetur</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Address:</td>
                                                <td>Ut enim ad minim veniam, quis nostrud</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Town/City:</td>
                                                <td>Eexercitation ullamco laboris nisi ut aliquip ex ea</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">State/Country:</td>
                                                <td>Commodo consequat. Duis aute irure dol</td>
                                            </tr>
                                            <tr>
                                                <td class="ttg-color--gray-dark">Postode/ZIP:</td>
                                                <td>88888</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <span>Account Details <i class="icon-down-open"></i></span>
                                <div class="tt-tabs__content">
                                    <form action="#">
                                        <div class="tt-form">
                                            <div class="tt-form__form">
                                                <label>
                                                    <div class="row">
                                                        <div class="col-md-3"><span
                                                                    class="ttg__required">First Name:</span></div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control colorize-theme6-bg"
                                                                   placeholder="Enter please your first name">
                                                        </div>
                                                    </div>
                                                </label>
                                                <label>
                                                    <div class="row">
                                                        <div class="col-md-3"><span
                                                                    class="ttg__required">Last Name:</span></div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control colorize-theme6-bg"
                                                                   placeholder="Enter please your last name">
                                                        </div>
                                                    </div>
                                                </label>
                                                <label>
                                                    <div class="row">
                                                        <div class="col-md-3"><span class="ttg__required">Email:</span>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control colorize-theme6-bg"
                                                                   placeholder="Enter please your name">
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="tt-tabs-my-account__form ttg-mt--40">
                                            <div class="tt-tabs-my-account__form_title">Password Change</div>
                                            <p>Current password (leave blank to leave unchanged)</p>
                                            <input type="text" class="form-control"
                                                   placeholder="Enter please your name">
                                            <p>Current password (leave blank to leave unchanged)</p>
                                            <input type="text" class="form-control"
                                                   placeholder="Enter please your name">
                                            <p>Current password (leave blank to leave unchanged)</p>
                                            <input type="text" class="form-control"
                                                   placeholder="Enter please your name">
                                        </div>
                                        <button class="btn ttg-mt--40">Save Changes</button>
                                    </form>
                                </div>
                            </div>
                            <div>
                                <span>Logout <i class="icon-down-open"></i></span>
                                <div class="tt-tabs__content">
                                    <h4>Logout</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
@endsection