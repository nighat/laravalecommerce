@extends ('layouts/mogo')


@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>Icon Box</h1>
                </div>

                <div class="ttg-mt--60">

                    <div class="tt-shp-info tt-shp-info__design-02 tt-shp-info__align--left">
                        <div class="row ttg-grid-padding--none ttg-grid-border ttg-grid-border-c--white">
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section tt-shp-info__align--left ">
                                    <i class="icon-box"></i>
                                    <div>
                                        <div class="tt-shp-info__strong">Free Shipping & Return</div>
                                        <p>Free shipping on all orders over $99.</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section tt-shp-info__align--left ">
                                    <i class="icon-thumbs-up-1"></i>
                                    <div>
                                        <div class="tt-shp-info__strong">Money Back Guarantee</div>
                                        <p>100% money back guarantee.</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section tt-shp-info__align--left ">
                                    <i class="icon-phone"></i>
                                    <div>
                                        <div class="tt-shp-info__strong">Online Support</div>
                                        <p>Free shipping on all orders over $99.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="tt-shp-info tt-shp-info__design-01">
                        <div class="row ttg-grid-padding--none ttg-grid-border">
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <i class="icon-phone"></i>
                                    <div class="tt-shp-info__strong">+(777) 2345 7885</div>
                                    <p>Toll-free hotline. 7 days a week from <strong><em>10.00 a.m. to 6.00
                                                p.m.</em></strong></p>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <i class="icon-box"></i>
                                    <div class="tt-shp-info__strong">Free Shipping</div>
                                    <p>Shipping prices for any form of delivery and order’s cost is constant - $49. A
                                        free shipping is available for orders <span>more than $99.</span></p>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <i class="icon-left"></i>
                                    <div class="tt-shp-info__strong">Returns and Exchanges</div>
                                    <p>Any goods, that was bought in our online store, can be returned during <span>30 days</span>
                                        since purchase date.</p>
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="tt-shp-info tt-shp-info__design-04 tt-shp-info__design-striped">
                        <div class="row ttg-grid-padding--none">
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <span class="tt-shp-info__number">1</span>
                                    <div class="tt-shp-info__strong">Adequate Prices</div>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <span class="tt-shp-info__number">2</span>
                                    <div class="tt-shp-info__strong">Ideal Balance Between Price & Quality.</div>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section ">
                                    <span class="tt-shp-info__number">3</span>
                                    <div class="tt-shp-info__strong">Saving of Time</div>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ttg-mt--90 ttg-mb--110">

                    <div class="tt-shp-info tt-shp-info__design-03 tt-shp-info__align--left">
                        <div class="row ttg-grid-padding--none">
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section tt-shp-info__align--left ">
                                    <i class="icon-box"></i>
                                    <div>
                                        <div class="tt-shp-info__strong">Free Shipping & Return</div>
                                        <p>Free shipping on all orders over $99.</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section tt-shp-info__align--left ">
                                    <i class="icon-thumbs-up-1"></i>
                                    <div>
                                        <div class="tt-shp-info__strong">Money Back Guarantee</div>
                                        <p>100% money back guarantee.</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4">
                                <a href="#" class="tt-shp-info__section tt-shp-info__align--left ">
                                    <i class="icon-phone"></i>
                                    <div>
                                        <div class="tt-shp-info__strong">Online Support</div>
                                        <p>Free shipping on all orders over $99.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection