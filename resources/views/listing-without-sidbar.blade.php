@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')
                <div class="tt-page__name"><h1>Blog</h1></div>
                <div class="tt-post ttg-image-translate--right">
                    <div class="tt-post__bg">
                        <img data-srcset="images/blog/without_sidebar/blog-without-sidebar-01.jpg" alt="Image name">
                    </div>
                    <div class="tt-post__content">
                        <div>
                            <div class="tt-post__category">
                                <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                            </div>
                            <a href="#" class="tt-post__comments">
                                <i class="icon-comment-empty"></i><span>7</span>
                            </a>
                        </div>
                        <div>
                            <a href="#" class="tt-post__title">Sed ut perspiciatis unde omnis iste natus error sit
                                voluptatem accusantium.</a>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                                voluptas.</p>
                            <div class="tt-post__info">
                                <span>Robert</span> on December 28, 2017
                                <div class="tt-post__tags">
                                    <i class="icon-tag-1"></i>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Audio,</a>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tt-post ttg-image-scale">
                    <div class="tt-post__bg">
                        <img data-srcset="images/blog/without_sidebar/blog-without-sidebar-02.jpg" alt="Image name">
                    </div>
                    <div class="tt-post__content">
                        <div>
                            <div class="tt-post__category">
                                <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                                <a href="index.html?page=listing-with-custom-html-block.html">Audio</a>
                            </div>
                            <a href="#" class="tt-post__comments">
                                <i class="icon-comment-empty"></i><span>7</span>
                            </a>
                        </div>
                        <div>
                            <a href="#" class="tt-post__title">Unde omnis iste natus error sit oluptatem
                                accusantium.</a>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                                voluptas.</p>
                            <div class="tt-post__info">
                                <span>Robert</span> on December 28, 2017
                                <div class="tt-post__tags">
                                    <i class="icon-tag-1"></i>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Audio,</a>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tt-post ttg-image-translate--left">
                    <div class="tt-post__bg">
                        <img data-srcset="images/blog/without_sidebar/blog-without-sidebar-03.jpg" alt="Image name">
                    </div>
                    <div class="tt-post__content">
                        <div>
                            <div class="tt-post__category">
                                <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                            </div>
                            <a href="#" class="tt-post__comments">
                                <i class="icon-comment-empty"></i><span>7</span>
                            </a>
                        </div>
                        <div>
                            <a href="#" class="tt-post__title">Voluptatem accusantium.</a>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                                voluptas.</p>
                            <div class="tt-post__info">
                                <span>Robert</span> on December 28, 2017
                                <div class="tt-post__tags">
                                    <i class="icon-tag-1"></i>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Audio,</a>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tt-post ttg-image-translate--top">
                    <div class="tt-post__bg">
                        <img data-srcset="images/blog/without_sidebar/blog-without-sidebar-04.jpg" alt="Image name">
                    </div>
                    <div class="tt-post__content">
                        <div>
                            <div class="tt-post__category">
                                <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                                <a href="index.html?page=listing-with-custom-html-block.html">Audio</a>
                            </div>
                            <a href="#" class="tt-post__comments">
                                <i class="icon-comment-empty"></i><span>7</span>
                            </a>
                        </div>
                        <div>
                            <a href="#" class="tt-post__title">Doloremque laudantium, totam rem aperiam, eaque ipsa.</a>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                                voluptas.</p>
                            <div class="tt-post__info">
                                <span>Robert</span> on December 28, 2017
                                <div class="tt-post__tags">
                                    <i class="icon-tag-1"></i>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Audio,</a>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tt-post ttg-image-scale">
                    <div class="tt-post__bg">
                        <img data-srcset="images/blog/without_sidebar/blog-without-sidebar-05.jpg" alt="Image name">
                    </div>
                    <div class="tt-post__content">
                        <div>
                            <div class="tt-post__category">
                                <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                            </div>
                            <a href="#" class="tt-post__comments">
                                <i class="icon-comment-empty"></i><span>7</span>
                            </a>
                        </div>
                        <div>
                            <a href="#" class="tt-post__title">Perspiciatis unde omnis iste natus error sit
                                voluptatem.</a>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                                voluptas.</p>
                            <div class="tt-post__info">
                                <span>Robert</span> on December 28, 2017
                                <div class="tt-post__tags">
                                    <i class="icon-tag-1"></i>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Audio,</a>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tt-post ttg-image-translate--bottom">
                    <div class="tt-post__bg">
                        <img data-srcset="images/blog/without_sidebar/blog-without-sidebar-06.jpg" alt="Image name">
                    </div>
                    <div class="tt-post__content">
                        <div>
                            <div class="tt-post__category">
                                <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                                <a href="index.html?page=listing-with-custom-html-block.html">Audio</a>
                            </div>
                            <a href="#" class="tt-post__comments">
                                <i class="icon-comment-empty"></i><span>7</span>
                            </a>
                        </div>
                        <div>
                            <a href="#" class="tt-post__title">Sit voluptatem accusantium doloremque laudantium, totam
                                rem aperiam.</a>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                                voluptas.</p>
                            <div class="tt-post__info">
                                <span>Robert</span> on December 28, 2017
                                <div class="tt-post__tags">
                                    <i class="icon-tag-1"></i>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Audio,</a>
                                    <a href="index.html?page=listing-with-custom-html-block.html">Headphones</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tt-page__pagination">
                    <div class="tt-pagination">
                        <a href="#" class="btn tt-pagination__prev ttg-hidden">Prev</a>
                        <div class="tt-pagination__numbs">
                            <span>1</span>
                            <span><a href="#">2</a></span>
                            <span><a href="#">3</a></span>
                        </div>
                        <a href="#" class="btn tt-pagination__next">Next</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
@endsection