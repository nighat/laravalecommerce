<aside class="tt-layout__sidebar tt-sticky-block">
    <div class="tt-layout__sidebar-sticky tt-sticky-block__inner">
        <div class="tt-sidebar">
            <div class="tt-sidebar__btn">
                <div class="tt-sidebar__btn-open">
                    <i class="icon-filter-1"></i><span>FILTER</span>
                </div>
                <div class="tt-sidebar__btn-close">
                    <i class="icon-cancel-1"></i><span>CLOSE</span>
                </div>
            </div>
            <div class="tt-sidebar__content">
                <div class="tt-layer-nav">
                    <div class="tt-layer-nav__title">Categories</div>
                    <ul class="tt-layer-nav__categories tt-categories tt-categories__toggle">
                        <li class="tt-categories__open active">
                            <a href="#">Audio <span>16</span></a>
                            <i class="tt-categories__next icon-up-open"></i>
                            <ul>
                                <li><a href="#">Wireless <span>3</span></a></li>
                                <li><a href="#">Built-In Microphone <span>3</span></a></li>
                                <li><a href="#">Bluetooth Enabled <span>4</span></a></li>
                                <li><a href="#">Rechargeable <span>4</span></a></li>
                                <li><a href="#">USB Device Charging <span>3</span></a>
                                    <i class="tt-categories__next icon-up-open"></i>
                                    <ul>
                                        <li><a href="#">Category #1 <span>3</span></a></li>
                                        <li><a href="#">Category #2 <span>3</span></a></li>
                                        <li><a href="#">Category #3 <span>3</span></a></li>
                                        <li><a href="#">Category #4 <span>5</span></a></li>
                                        <li><a href="#">Category #5 <span>7</span></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="tt-categories__open">
                            <a href="#">Watches <span>21</span></a>
                            <i class="tt-categories__next icon-up-open"></i>
                            <ul>
                                <li><a href="#">Clock Display <span>3</span></a></li>
                                <li><a href="#">Water Resistant <span>3</span></a></li>
                                <li><a href="#">Wireless Syncing <span>3</span></a></li>
                                <li><a href="#">Rechargeable <span>5</span></a></li>
                                <li><a href="#">GPS Enabled <span>7</span></a></li>
                            </ul>
                        </li>
                        <li class="tt-categories__open">
                            <a href="#">Trackers <span>13</span></a>
                            <i class="tt-categories__next icon-up-open"></i>
                            <ul>
                                <li><a href="#">Watch style <span>3</span></a></li>
                                <li><a href="#">Wrist <span>3</span></a></li>
                                <li><a href="#">Clothing <span>5</span></a></li>
                                <li><a href="#">Ankle <span>1</span></a></li>
                                <li><a href="#">Belt <span>1</span></a></li>
                            </ul>
                        </li>
                        <li class="tt-categories__open">
                            <a href="#">Power Banks <span>18</span></a>
                            <i class="tt-categories__next icon-up-open"></i>
                            <ul>
                                <li><a href="#">USB Port(s) <span>4</span></a></li>
                                <li><a href="#">USB Device Charging <span>3</span></a></li>
                                <li><a href="#">Overload Protection <span>4</span></a></li>
                                <li><a href="#">Rechargeable <span>6</span></a></li>
                                <li><a href="#">Wireless <span>1</span></a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="tt-layer-nav__title">Price</div>
                    <div class="tt-layer-nav__price-section">
                        <div class="tt-layer-nav__price-range"><input type="hidden" data-min="0" data-max="500">
                        </div>
                    </div>
                    <ul class="tt-layer-nav__price tt-categories tt-categories__toggle">
                        <li><a href="#">$10-$100 <span>25</span></a></li>
                        <li><a href="#">$100-$200 <span>7</span></a></li>
                        <li><a href="#">$200-$500 <span>14</span></a></li>
                    </ul>
                    <div class="tt-layer-nav__title">Size</div>
                    <ul class="tt-layer-nav__size tt-categories tt-categories__toggle">
                        <li><a href="#">XS <span>25</span></a></li>
                        <li><a href="#">S <span>7</span></a></li>
                        <li><a href="#">M <span>14</span></a></li>
                        <li><a href="#">L <span>14</span></a></li>
                        <li><a href="#">XL <span>14</span></a></li>
                        <li><a href="#">XXL <span>14</span></a></li>
                    </ul>
                    <div class="tt-layer-nav__title">Color</div>
                    <ul class="tt-layer-nav__color tt-categories tt-categories__toggle">
                        <li>
                            <a href="#" class="color-blacks">
                                <i><img src="images/colors/color-01.png" alt="Image name"></i>
                                Brown
                                <span>25</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="color-blue">
                                <i><img src="images/colors/color-02.png" alt="Image name"></i>
                                Gray
                                <span>7</span></a>
                        </li>
                        <li>
                            <a href="#" class="color-brown">
                                <i><img src="images/colors/color-03.png" alt="Image name"></i>
                                White
                                <span>1</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="color-gray">
                                <i><img src="images/colors/color-04.png" alt="Image name"></i>
                                Black
                                <span>4</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tt-layer-nav__title">Tags</div>
                    <div class="tt-layer-nav__tags">
                        <a href="#">Clock Display</a>
                        <a href="#">Water Resistant</a>
                        <a href="#">Wireless Syncing</a>
                        <a href="#" class="active">Rechargeable</a>
                        <a href="#">GPS Enabled</a>
                        <a href="#">Trackers</a>
                        <a href="#">Watch style</a>
                        <a href="#">Wrist</a>
                        <a href="#">Clothing</a>
                        <a href="#">Ankle</a>
                        <a href="#">Belt</a>
                        <a href="#">Power Banks</a>
                    </div>
                    <div class="tt-layer-nav__title">New Products</div>
                    <div class="tt-layer-nav__new tt-layer-nav__product">
                        <div class="tt-layer-nav__product-section">
                            <div class="tt-layer-nav__product-image">
                                <a href="#">
                                    <img src="images/products/product-01.jpg" alt="Image name">
                                    <span class="tt-layer-nav__product-label tt-label__new">New</span>
                                </a>
                            </div>
                            <div class="tt-layer-nav__product-info">
                                <a href="#"><p>Elegant and fresh. A most attractive mobile...</p></a>
                                <div class="tt-layer-nav__product-price">
                                    <div class="tt-price">
                                        <span>$25</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tt-layer-nav__product-section">
                            <div class="tt-layer-nav__product-image">
                                <a href="#">
                                    <img src="images/products/product-02.jpg" alt="Image name">
                                    <span class="tt-layer-nav__product-label tt-label__new">New</span>
                                </a>
                            </div>
                            <div class="tt-layer-nav__product-info">
                                <a href="#"><p>Elegant and fresh. A most attractive mobile...</p></a>
                                <div class="tt-layer-nav__product-price">
                                    <div class="tt-price tt-price--sale">
                                        <span>$19</span><span>$25</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tt-layer-nav__product-section">
                            <div class="tt-layer-nav__product-image">
                                <a href="#">
                                    <img src="images/products/product-03.jpg" alt="Image name">
                                    <span class="tt-layer-nav__product-label tt-label__new">New</span>
                                </a>
                            </div>
                            <div class="tt-layer-nav__product-info">
                                <a href="#"><p>Elegant and fresh. A most attractive mobile...</p></a>
                                <div class="tt-layer-nav__product-price">
                                    <div class="tt-price">
                                        <span>$35</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tt-layer-nav__title">Special Products</div>
                    <div class="tt-layer-nav__sale tt-layer-nav__product">
                        <div class="tt-layer-nav__product-section">
                            <div class="tt-layer-nav__product-image">
                                <a href="#">
                                    <img src="images/products/product-04.jpg" alt="Image name">
                                    <span class="tt-layer-nav__product-label tt-label__sale">Sale</span>
                                </a>
                            </div>
                            <div class="tt-layer-nav__product-info">
                                <a href="#"><p>Elegant and fresh. A most attractive mobile...</p></a>
                                <div class="tt-layer-nav__product-price">
                                    <div class="tt-price tt-price--sale">
                                        <span>$45</span><span>$54</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tt-layer-nav__product-section">
                            <div class="tt-layer-nav__product-image">
                                <a href="#">
                                    <img src="images/products/product-05.jpg" alt="Image name">
                                    <span class="tt-layer-nav__product-label tt-label__sale">Sale</span>
                                </a>
                            </div>
                            <div class="tt-layer-nav__product-info">
                                <a href="#"><p>Elegant and fresh. A most attractive mobile...</p></a>
                                <div class="tt-layer-nav__product-price">
                                    <div class="tt-price">
                                        <span>$40</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tt-layer-nav__product-section">
                            <div class="tt-layer-nav__product-image">
                                <a href="#">
                                    <img src="images/products/product-06.jpg" alt="Image name">
                                    <span class="tt-layer-nav__product-label tt-label__sale">Sale</span>
                                </a>
                            </div>
                            <div class="tt-layer-nav__product-info">
                                <a href="#"><p>Elegant and fresh. A most attractive mobile...</p></a>
                                <div class="tt-layer-nav__product-price">
                                    <div class="tt-price tt-price--sale">
                                        <span>$19</span><span>$25</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tt-layer-nav__title">Compare Products</div>
                    <div class="tt-layer-nav__compare tt-layer-nav__prod-list">
                        <div class="tt-layer-nav__prod-list-info">
                            <p>You have <span>2 items</span> to compare.</p>
                        </div>
                        <div class="tt-layer-nav__prod-list-section">
                            <a href="#"><p>Elegant and fresh. A most attractive mobile...</p></a>
                            <a href="#" class="tt-layer-nav__prod-list-close"><i class="icon-cancel-1"></i></a>
                        </div>
                        <div class="tt-layer-nav__prod-list-section">
                            <a href="#"><p>Elegant and fresh. A most attractive mobile...</p></a>
                            <a href="#" class="tt-layer-nav__prod-list-close"><i class="icon-cancel-1"></i></a>
                        </div>
                    </div>
                    <div class="tt-layer-nav__title">My Wishlist</div>
                    <div class="tt-layer-nav__wishlist">
                        <div class="tt-layer-nav__prod-list-info">
                            <p>You have no items in your wish list.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tt-sidebar__bg"></div>
    </div>
</aside>