@extends('layouts/mogo')

@section('main_content')
    <div class="panel-heading">
        <h5 style="text-align: center">Cart Lists <a href="{{ route('cart.create') }}" class=" btn btn-danger pull-right">Add New cart</a></h5>
        <h5 >{{ session('message') }}</h5>


    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Session Id </th>
            <th>Product Id</th>
            <th>Product Qty</th>
            <th>Product Price</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>

        @foreach($carts as $cart)

            <tr>
                <td>{{ $loop->index + 1}}</td>
                
                <td>{{ $cart->session_id }}</td>
                <td>{{ $cart->product_id }}</td>
                <td>{{ $cart->product_qty}}</td>
                <td>{{ $cart->product_price}}</td>
                <td> <a href="{{ route('cart.show',$cart->id) }}" class=" btn btn-danger"><span class=" glyphicon glyphicon-eye-open"></span></a>
                    <a href="{{ route('cart.edit',$cart->id) }}" class=" btn btn-info"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $cart->id }}" method="POST" action="{{ route('cart.destroy',$cart->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $cart->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-info"><span class="glyphicon glyphicon-trash"></span></a>

                </td>


            </tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th>S.No</th>
            <th>Session Id </th>
            <th>Product Id</th>
            <th>Product Qty</th>
            <th>Product Price</th>
            <th>Action</th>



        </tr>
        </tfoot>
    </table>



@endsection