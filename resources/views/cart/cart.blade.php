@extends('layouts/mogo')

@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                    <div class="panel-heading"><h3 style="text-align: center">Cart Create Form</h3>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('cart.store') }}">
                            {{ csrf_field() }}

                             <div class="form-group">
                                <label for="session_id" class="col-md-4 control-label">Session_Id</label>

                                <div class="col-md-6">
                                    <input id="session_id" type="number" class="form-control" name="session_id" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="product_id" class="col-md-4 control-label">product_id</label>

                                <div class="col-md-6">
                                    <input id="product_id" type="number" class="form-control" name="product_id" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="product_qty" class="col-md-4 control-label">product_qty</label>

                                <div class="col-md-6">
                                    <input id="product_qty" type="number" class="form-control" name="product_qty" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="product_price" class="col-md-4 control-label">Product Price</label>

                                <div class="col-md-6">
                                    <input id="product_price" type="number" class="form-control" name="product_price" >
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('cart.index') }}" class=" btn btn-danger">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>

            </div>
        </div>
    </div>
@endsection