@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')

                <div class="tt-page__name text-center">
                    <h1>Site Map</h1>
                </div>

                <div class="tt-sitemap">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                                <h5>Home</h5>
                                <ul>
                                    <li><a href="index.html">Home — Variant 1</a></li>
                                    <li><a href="index-02.html">Home — Variant 2</a></li>
                                    <li><a href="index-03.html">Home — Variant 3</a></li>
                                    <li><a href="index-04.html">Home — Variant 4</a></li>
                                    <li><a href="index-05.html">Home — Variant 5</a></li>
                                    <li><a href="index-06.html">Home — Variant 6</a></li>
                                    <li><a href="index-07.html">Home — Variant 7</a></li>
                                    <li><a href="index-08.html">Home — Variant 8</a></li>
                                    <li><a href="index-09.html">Home — Variant 9</a></li>
                                    <li><a href="index-10.html">Home — Variant 10</a></li>
                                    <li><a href="index-11.html">Home — Variant 11</a></li>
                                    <li><a href="index-12.html">Home — Variant 12</a></li>
                                </ul>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                                <h5>Listing</h5>
                                <ul>
                                    <li><a href="listing-collections.html">Listing Collections</a></li>
                                    <li><a href="listing-with-custom-html-block.html">Listing with
                                            Custom HTML Block</a></li>
                                    <li><a href="listing-catalogue.html">Listing Catalogue</a></li>
                                    <li><a href="listing-with-sidebar.html">Listing with Sidebar</a>
                                    </li>
                                    <li><a href="listing-right-sidebar.html">Listing Right Sidebar</a>
                                    </li>
                                    <li><a href="listing-without-columns.html">Listing without
                                            Columns</a></li>
                                    <li><a href="listing-fullwidth.html">Listing Fullwidth</a></li>
                                    <li><a href="listing-big-reviews.html">Listing Big Previews</a></li>
                                    <li><a href="listing.html">Listing Show All</a></li>
                                </ul>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                                <h5>Product</h5>
                                <ul>
                                    <li><a href="product-simple-variant-1.html">Simple Product - Variant
                                            1</a></li>
                                    <li><a href="product-simple-variant-2.html">Simple Product - Variant
                                            2</a></li>
                                    <li><a href="iproduct-variable.html">Variable Product</a></li>
                                    <li><a href="iproduct-grouped.html">Grouped product</a></li>
                                    <li><a href="iproduct-out-of-stock.html">Out of stock product</a>
                                    </li>
                                    <li><a href="product-on-sale.html">On sale product</a></li>
                                    <li><a href="product.html">Product All</a></li>
                                </ul>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                                <h5>Blog</h5>
                                <ul>
                                    <li><a href="blog-listing-with-sidebar.html">Listing with
                                            Sidebar</a></li>
                                    <li><a href="blog-listing-without-sidebar.html">Listing without
                                            Sidebar</a></li>
                                    <li><a href="blog-grid.html">Grid</a></li>
                                    <li><a href="blog-masonry.html">Masonry</a></li>
                                    <li><a href="blog-standart-post.html">Post Formats</a>
                                        <ul>
                                            <li><a href="blog-standart-post.html">Standard post</a></li>
                                            <li><a href="blog-gallery-post.html">Gallery Post</a></li>
                                            <li><a href="blog-video-post.html">Video Post</a></li>
                                            <li><a href="blog-text-post.html">Text Post</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                                <h5>Gallery</h5>
                                <ul>
                                    <li><a href="gallery-masonry.html">Masonry Style</a></li>
                                    <li><a href="gallery-3-thumbs.html">3 Equal Thumbs</a></li>
                                    <li><a href="gallery-4-thumbs.html">4 Equal Thumbs</a></li>
                                </ul>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                                <h5>Pages</h5>
                                <ul>
                                    <li><a href="contacts.html">Contacts</a></li>
                                    <li><a href="about.html">About</a></li>
                                    <li><a href="faq.html">FAQ</a></li>
                                    <li><a href="sitemap.html">Sitemap</a></li>
                                    <li><a href="services.html">Services</a></li>
                                    <li><a href="coming-soon.html">Coming Soon</a></li>
                                </ul>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                                <h5>Empty Pages</h5>
                                <ul>
                                    <li><a href="404.html">404</a></li>
                                    <li><a href="empty-category.html">Empty Category</a></li>
                                    <li><a href="empty-compare.html">Empty Compare</a></li>
                                    <li><a href="empty-search.html">Empty Search</a></li>
                                    <li><a href="empty-shopping-cart.html">Empty Shopping-cart</a></li>
                                    <li><a href="empty-wishlist.html">Empty Wishlist</a></li>
                                </ul>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                                <h5>Elements</h5>
                                <ul>
                                    <li><a href="typography.html"" >Typography</a></li>
                                    <li><a href="#">Banners</a></li>
                                    <li><a href="#">Tabs</a></li>
                                    <li><a href="#">Accordion</a></li>
                                    <li><a href="#">FAQs / Toggles</a></li>
                                    <li><a href="#">Social Media Profiles</a></li>
                                    <li><a href="#">Buttons</a></li>
                                    <li><a href="#">Icon Box</a></li>
                                    <li><a href="#">Progress Bars</a></li>
                                    <li><a href="#">Google Maps</a></li>
                                </ul>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                                <h5>Collections</h5>
                                <ul>
                                    <li>
                                        <a href="#">Audio</a>
                                        <ul>
                                            <li><a href="#">Wireless</a></li>
                                            <li><a href="#">Built-In Microphone</a></li>
                                            <li><a href="#">Bluetooth Enabled</a></li>
                                            <li><a href="#">Rechargeable</a></li>
                                            <li><a href="#">USB Device Charging</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Watches</a>
                                        <ul>
                                            <li><a href="#">Clock Display</a></li>
                                            <li><a href="#">Water Resistant</a></li>
                                            <li><a href="#">Wireless Syncing</a></li>
                                            <li><a href="#">Rechargeable</a></li>
                                            <li><a href="#">GPS Enabled</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Trackers</a>
                                        <ul>
                                            <li><a href="#">Watch style</a></li>
                                            <li><a href="#">Wrist</a></li>
                                            <li><a href="#">Clothing</a></li>
                                            <li><a href="#">Ankle</a></li>
                                            <li><a href="#">Belt</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Power Banks</a>
                                        <ul>
                                            <li><a href="#">USB Port(s)</a></li>
                                            <li><a href="#">USB Device Charging</a></li>
                                            <li><a href="#">Overload Protection</a></li>
                                            <li><a href="#">Rechargeable</a></li>
                                            <li><a href="#">Wireless</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
@endsection