@extends('layouts/mogo')

<!-- MAIN -->
@section('main_content')

<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent tt-layout__fullwidth">
        <div class="tt-layout__content">
            <div class="container">
                <div class="tt-sr tt-sr__nav--off" data-layout="fullscreen">
                    <div class="tt-sr__content" data-version="5.3.0.2">
                        <ul>
                            <li data-index="rs-3045"
                                data-transition="parallaxvertical"
                                data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
                                data-masterspeed="1500">
                                <img src="images/slider/10/slide-01.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                            rs-parallaxlevel-3
                            tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[-100%];opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Headphone</div>
                                    <span>$127</span>
                                    <p>Superior sound quality</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3046"
                                data-transition="fade"
                                data-masterspeed="1500">
                                <img src="images/slider/10/slide-02.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-kenburns="on"
                                     data-duration="10000"
                                     data-ease="Linear.easeNone"
                                     data-scalestart="120"
                                     data-scaleend="100"
                                     data-offsetstart="0 0"
                                     data-offsetend="0 0"
                                     data-rotatestart="0"
                                     data-rotateend="0"
                                     data-bgparallax="8">
                                <div class="tp-caption
                        rs-parallaxlevel-3
                        tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"1999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Speaker Bottle</div>
                                    <span>$121</span>
                                    <p>Portable Bluetooth stereo speakers</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3047"
                                data-transition="parallaxhorizontal"
                                data-masterspeed="1500">
                                <img src="images/slider/10/slide-03.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                        rs-parallaxlevel-3
                        tt-sr__design-white
                        tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Tracker</div>
                                    <span>$65</span>
                                    <p>Cheap Fitness Tracker</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3048"
                                data-transition="zoomout"
                                data-masterspeed="1500">
                                <img src="images/slider/10/slide-04.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                        rs-parallaxlevel-3
                        tt-sr__text
                        tt-sr__design-white"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"1999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Smart Watches</div>
                                    <span>$349</span>
                                    <p>Just what you need. Right when you need it.</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3049"
                                data-transition="parallaxvertical"
                                data-masterspeed="1500">
                                <img src="images/slider/10/slide-05.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-bgparallax="8">
                                <div class="tp-caption
                        rs-parallaxlevel-3
                        tt-sr__text"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];opacity:0;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Earphone</div>
                                    <span>$82</span>
                                    <p>Superior sound quality</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                            <li data-index="rs-3050"
                                data-transition="fade"
                                data-masterspeed="1500">
                                <img src="images/slider/10/slide-06.jpg"
                                     alt="Image name"
                                     class="rev-slidebg"
                                     data-bgposition="center center"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat"
                                     data-kenburns="on"
                                     data-duration="6000"
                                     data-ease="Linear.easeNone"
                                     data-scalestart="120"
                                     data-scaleend="100"
                                     data-offsetstart="0 0"
                                     data-offsetend="0 0"
                                     data-rotatestart="0"
                                     data-rotateend="0"
                                     data-bgparallax="8">
                                <div class="tp-caption
                        rs-parallaxlevel-3
                        tt-sr__text
                        tt-sr__design-white"
                                     data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                     data-x="left"
                                     data-y="center"
                                     data-whitespace="nowrap"
                                     data-width="['auto']"
                                     data-height="['auto']"
                                     data-hoffset="76">
                                    <div>Power Bank</div>
                                    <span>$49</span>
                                    <p>Elegant and fresh. A most attractive mobile power
                                        supply.</p>
                                    <a href="#" class="tt-btn tt-btn--big tt-btn--hover-effect colorize-btn7">
                                        <i class="icon-shop24"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="tt-home__promobox-01">

                    <div class="container-fluid ttg-cont-padding--none">
                        <div class="row ttg-grid-padding--none">
                            <div class="col-sm-8">
                                <div class="row ttg-grid-padding--none">
                                    <div class="col-sm-6">
                                        <a href="#" class="tt-promobox
                                                           ttg-text-animation-parent
                                                           ttg-image-translate--right
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md">
                                            <div class="tt-promobox__content">
                                                <img data-srcset="images/promoboxes/promobox-17.jpg" alt="Image name">
                                                <div class="tt-promobox__text"
                                                     data-resp-md="md"
                                                     data-resp-sm="lg"
                                                     data-resp-xs="sm">
                                                    <div class="colorize-theme2-c">Watches</div>
                                                </div>
                                                <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                    <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                                    <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                        <div class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c">Watches</span>
                                                        </div>
                                                        <p class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c"><span>28</span> products</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="#" class="tt-promobox
                                                           ttg-text-animation-parent
                                                           ttg-image-translate--top
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md">
                                            <div class="tt-promobox__content">
                                                <img data-srcset="images/promoboxes/promobox-18.jpg" alt="Image name">
                                                <div class="tt-promobox__text"
                                                     data-resp-md="md"
                                                     data-resp-sm="lg"
                                                     data-resp-xs="sm">
                                                    <div class="colorize-theme2-c">Trackers</div>
                                                </div>
                                                <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                    <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                                    <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                        <div class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c">Trackers</span>
                                                        </div>
                                                        <p class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c"><span>46</span> products</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-12">
                                        <a href="#" class="tt-promobox
                                                           tt-promobox__size-wide
                                                           ttg-text-animation-parent
                                                           ttg-image-scale
                                                           ttg-animation-disable--md
                                                           tt-promobox__hover-disable--md">
                                            <div class="tt-promobox__content">
                                                <img data-srcset="images/promoboxes/promobox-19.jpg" alt="Image name">
                                                <div class="tt-promobox__text"
                                                     data-resp-md="md"
                                                     data-resp-sm="lg"
                                                     data-resp-xs="sm">
                                                    <div class="colorize-theme2-c">Headphones</div>
                                                </div>
                                                <div class="tt-promobox__hover tt-promobox__hover--fade">
                                                    <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                                    <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                        <div class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c">Headphones</span>
                                                        </div>
                                                        <p class="ttg-text-animation--emersion">
                                                            <span class="colorize-theme2-c">32 products</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <a href="#" class="tt-promobox
                                                   tt-promobox__size-high
                                                   ttg-text-animation-parent
                                                   ttg-image-translate--left
                                                   ttg-animation-disable--md
                                                   tt-promobox__hover-disable--md">
                                    <div class="tt-promobox__content">
                                        <img data-srcset="images/promoboxes/promobox-20.jpg" alt="Image name">
                                        <div class="tt-promobox__text"
                                             data-resp-md="md"
                                             data-resp-sm="lg"
                                             data-resp-xs="sm">
                                            <div class="colorize-theme2-c">Earphones</div>
                                        </div>
                                        <div class="tt-promobox__hover tt-promobox__hover--fade">
                                            <div class="tt-promobox__hover-bg colorize-theme4-bg"></div>
                                            <div class="tt-promobox__text tt-promobox__point-lg--center">
                                                <div class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">Earphones</span>
                                                </div>
                                                <p class="ttg-text-animation--emersion">
                                                    <span class="colorize-theme2-c">24 products</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>
@endsection