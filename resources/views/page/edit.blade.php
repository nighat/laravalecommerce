@extends('layouts/mogo')

@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel-heading"><h5 style="text-align: center">Page Edit Form</h5></div>


                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('page.update',$page->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        <div class="form-group">
                            <label for="page_title" class="col-md-4 control-label">Page Title</label>

                            <div class="col-md-6">
                                <input id="page_title" type="text" class="form-control" name="page_title" value="{{ $page->page_title }}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="page_contant" class="col-md-4 control-label">Page Contant</label>

                            <div class="col-md-6">
                                <input id="page_contant" type="text" class="form-control" name="page_contant" value="{{ $page->page_contant }}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="created_by" class="col-md-4 control-label">Created By</label>

                            <div class="col-md-6">
                                <input id="created_by" type="text" class="form-control" name="created_by" value="{{ $page->created_by }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="updated_by" class="col-md-4 control-label">Updated By</label>

                            <div class="col-md-6">
                                <input id="updated_by" type="text" class="form-control" name="updated_by" value="{{ $page->updated_by }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">

                                <button type="submit" class="btn btn-primary">Submit</button>

                                <a href="{{ route('page.create') }}" class=" btn btn-danger">Create</a>
                                <a href="{{ route('page.index') }}" class=" btn btn-danger">page</a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection