

@extends('layouts/mogo')

@section('main_content')
       <div class="container">
              <div class="row">
                     <div class="col-md-10 col-md-offset-2">


                                   <div class="panel-heading ">
                                          <h5 style="text-align: center">Page Details
                                                 <a href="{{ route('page.create') }}" class=" btn btn-danger pull-right">Create</a>
                                                 <a href="{{ route('page.index') }}" class=" btn btn-danger pull-right">Page</a>
                                          </h5>

                                   </div>


                                   <div> <b>Page Title:-</b>  {{ $page->page_title}}</div>
                                   <div> <b>Page Contant:-</b>  {{ $page->page_contant}}</div>
                                   <div> <b>Create By:-</b> {{ $page->created_by}}</div>
                                  <div> <b>Update By:-</b>  {{ $page->updated_by}}</div>


                   </div>

              </div>
       </div>





@endsection