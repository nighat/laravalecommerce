@extends('layouts/mogo')

@section('main_content')
    <div class="panel-heading">
        <h5 style="text-align: center">Subscriber Lists <a href="{{ route('subscriber.create') }}" class=" btn btn-danger pull-right">Add New Subscriber</a></h5>
        <h5 >{{ session('message') }}</h5>


    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Mail</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>

        @foreach($subscribers as $subscriber)

            <tr>
                <td>{{ $loop->index + 1}}</td>
                <td>{{ $subscriber->mail}}</td>
                <td> <a href="{{ route('subscriber.show',$subscriber->id) }}" class=" btn btn-danger"><span class=" glyphicon glyphicon-eye-open"></span></a>
                    <a href="{{ route('subscriber.edit',$subscriber->id) }}" class=" btn btn-info"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $subscriber->id }}" method="POST" action="{{ route('subscriber.destroy',$subscriber->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $subscriber->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-info"><span class="glyphicon glyphicon-trash"></span></a>

                </td>


            </tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th>S.No</th>

            <th>Mail</th>
            <th>Action</th>



        </tr>
        </tfoot>
    </table>



@endsection