@extends ('layouts/mogo')


@section('main_content')
<!-- MAIN -->
<main>

    <div class="tt-layout tt-sticky-block__parent ">
        <div class="tt-layout__content">
            <div class="container">
                @include('include/breadcrumbs')


                <div class="tt-page__name text-center">
                    <h1>Compare</h1>
                </div>

                <div class="tt-compare">
                    <table>
                        <tr>
                            <th>
                                <p>Products</p>
                            </th>
                            <td>
                                <div class="tt-compare__product_del"><a href="#"><i class="icon-trash"></i></a></div>
                                <div class="tt-compare__product_image">
                                    <a href="#"><img src="images/compare/compare-01.jpg" alt="Image name"></a>
                                </div>
                                <div class="tt-compare__product_name">
                                    <a href="#"><p>Elegant and fresh. A most attractive mobile power supply.</p></a>
                                </div>
                                <div class="tt-compare__product_price">
                                    <div class="tt-price">
                                        <span>$25</span>
                                    </div>
                                </div>
                                <div class="tt-compare__product_to-cart">
                                    <a href="#" class="tt-btn tt-btn--cart colorize-btn6">
                                        <i class="icon-shop24"></i>
                                        <span>Add to Cart</span>
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div class="tt-compare__product_del"><a href="#"><i class="icon-trash"></i></a></div>
                                <div class="tt-compare__product_image">
                                    <a href="#"><img src="images/compare/compare-01.jpg" alt="Image name"></a>
                                </div>
                                <div class="tt-compare__product_name">
                                    <a href="#"><p>Elegant and fresh. A most attractive mobile power supply.</p></a>
                                </div>
                                <div class="tt-compare__product_price">
                                    <div class="tt-price tt-price--sale">
                                        <span>$32</span>
                                        <span>$38</span>
                                    </div>
                                </div>
                                <div class="tt-compare__product_to-cart">
                                    <a href="#" class="tt-btn tt-btn--cart colorize-btn6">
                                        <i class="icon-shop24"></i>
                                        <span>Add to Cart</span>
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div class="tt-compare__product_del"><a href="#"><i class="icon-trash"></i></a></div>
                                <div class="tt-compare__product_image">
                                    <a href="#"><img src="images/compare/compare-01.jpg" alt="Image name"></a>
                                </div>
                                <div class="tt-compare__product_name">
                                    <a href="#"><p>Elegant and fresh. A most attractive mobile power supply.</p></a>
                                </div>
                                <div class="tt-compare__product_price">
                                    <div class="tt-price">
                                        <span>$38</span>
                                    </div>
                                </div>
                                <div class="tt-compare__product_to-cart">
                                    <a href="#" class="tt-btn tt-btn--cart colorize-btn6">
                                        <i class="icon-shop24"></i>
                                        <span>Add to Cart</span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <p>Description</p>
                            </th>
                            <td>
                                <div class="tt-compare__product_description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                        veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                                        voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                        consequuntur magni dolores</p>
                                </div>
                            </td>
                            <td>
                                <div class="tt-compare__product_description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                        veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                                        voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                        consequuntur magni dolores</p>
                                </div>
                            </td>
                            <td>
                                <div class="tt-compare__product_description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                        veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                                        voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                        consequuntur magni dolores</p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><p>Availability</p></th>
                            <td>
                                <div class="tt-compare__product_param tt-compare__product_status"><span
                                            class="colorize-success-c">In Stock</span></div>
                            </td>
                            <td>
                                <div class="tt-compare__product_param tt-compare__product_status"><span
                                            class="colorize-error-c">Out Stock</span></div>
                            </td>
                            <td>
                                <div class="tt-compare__product_param tt-compare__product_status"><span
                                            class="colorize-success-c">In Stock</span></div>
                            </td>
                        </tr>
                        <tr>
                            <th><p>Color</p></th>
                            <td>
                                <div class="tt-compare__product_param"><span>White</span></div>
                            </td>
                            <td>
                                <div class="tt-compare__product_param"><span>White</span></div>
                            </td>
                            <td>
                                <div class="tt-compare__product_param"><span>White</span></div>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td class="tt-compare__product_param">
                                <div class="tt-compare__product_price">
                                    <div class="tt-price">
                                        <span>$25</span>
                                    </div>
                                </div>
                            </td>
                            <td class="tt-compare__product_param">
                                <div class="tt-compare__product_price">
                                    <div class="tt-price">
                                        <span>$25</span>
                                    </div>
                                </div>
                            </td>
                            <td class="tt-compare__product_param">
                                <div class="tt-compare__product_price">
                                    <div class="tt-price">
                                        <span>$25</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="tt-add-to-cart" data-active="true">
        <i class="icon-check"></i>
        <p>Added to Cart Successfully!</p>
        <a href="#" class="btn"><i class="icon-shop24"></i>View Cart</a>
    </div>

    <div class="tt-newsletter-popup" data-active="true">
        <div class="tt-newsletter-popup__text-01">
            <span>15</span>
            <span>
            <span>%</span>
            <span>off</span>
        </span>
        </div>
        <div class="tt-newsletter-popup__text-02"><p>Your Next Purchase When You Sign Up.</p></div>
        <p>By signing up, you accept the terms & Privacy Policy.</p>
        <div class="ttg-mb--30">
            <form action="#" class="tt-newsletter tt-newsletter--style-02">
                <input type="email" name="email" class="form-control" placeholder="Enter please your e-mail">
                <button type="submit" class="btn">
                    <i class="tt-newsletter__text-wait"></i>
                    <span class="tt-newsletter__text-default">Subscribe!</span>
                    <span class="tt-newsletter__text-error"><i class="icon-exclamation"></i>Please provide a valid email address!</span>
                    <span class="tt-newsletter__text-complete"><i class="icon-ok"></i>Check your inbox!</span>
                </button>
            </form>
        </div>
        <div class="tt-newsletter-popup__social">
            <div class="tt-social-icons tt-social-icons--style-03">
                <a href="#" class="tt-btn">
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-gplus"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-instagram-1"></i>
                </a>
                <a href="#" class="tt-btn">
                    <i class="icon-youtube-play"></i>
                </a>
            </div>
        </div>
        <label class="tt-newsletter-popup__show_popup tt-checkbox">
            <input type="checkbox" name="show-nawslatter">
            <span></span>
            Don't show this popup again
        </label>
    </div>
</main>

@endsection