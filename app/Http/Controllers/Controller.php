<?php

namespace App\Http\Controllers;

use App\Page;
use App\Product;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;




    public function __construct()
    {
        session_start();
        if (isset($_SESSION['guest_id'])) {
           echo $session = $_SESSION['guest_id']=rand().'-'.time;
        }
        else {
            $session = "";
        }
        //its just a dummy data object.
        $pages = Page::all();

        // Sharing is caring
        \View::share('pages', $pages);
    }
}
